import { Component, OnInit } from '@angular/core';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app works!';
  courses$:FirebaseListObservable<any>;
  lesson$:FirebaseObjectObservable<any>;
  lastCourse:any;


  constructor(private af: AngularFire) {

    this.courses$ = af.database.list('courses');
    this.courses$.subscribe(console.log);
    this.lesson$ = af.database.object('lessons/-Kd5JuxHKFAlC680YzPx');
    this.lesson$.subscribe(console.log);
    this.courses$.map(courses => courses[3])
      .subscribe(
        course => this.lastCourse = course
      );

    const courses$ : FirebaseListObservable<any> = af.database.list('courses');

    const course$ = af.database.object('courses/-Kd5JuxEeojEXknub0xZ');
    
    course$.subscribe(console.log);
  }

  listPush() {
    this.courses$.push({description:'TEST NEW COURSE'})
      .then(
        () => console.log('List push done!'),
        console.error
        );
  }

  listRemove() {
    this.courses$.remove(this.lastCourse);
  }


  listUpdate() {
    this.courses$.update(this.lastCourse, {description:"I have updated the Description"})
  }


  objUpdate() {
    this.lesson$.update({description: "NEW DESCRIPTION"})
  }



  objSet() {
    this.lesson$.set({description: "NEW DESCRIPTION"})
  }

  objRemove() {
    this.lesson$.remove()
  }

  add() {
    
  }


}
