import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { MaterialModule } from '@angular/material';
import 'hammerjs';
import { AngularFireModule } from 'angularfire2';



import { AppComponent } from './app.component';

export const firebaseConfig = {
    apiKey: "AIzaSyA-arnhXGZkWheI5b4IY8kK3UWTbustQKQ",
    authDomain: "angular-firebase-app-sta-ec699.firebaseapp.com",
    databaseURL: "https://angular-firebase-app-sta-ec699.firebaseio.com",
    storageBucket: "angular-firebase-app-sta-ec699.appspot.com",
    messagingSenderId: "533077895847"
};

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    MaterialModule.forRoot(),
    AngularFireModule.initializeApp(firebaseConfig)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
