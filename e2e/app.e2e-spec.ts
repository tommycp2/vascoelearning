import { VascoelearningPage } from './app.po';

describe('vascoelearning App', function() {
  let page: VascoelearningPage;

  beforeEach(() => {
    page = new VascoelearningPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
